#include "SonarSensor.h"

/** Returns range of given index
* @param index index of wanted range 
* @return range of index
*/
float SonarSensor::getRange(int index)
{
	this->updateSensor();
	if (index < 0 || index > 15)
		throw std::string("index must be between 0-15");
	else
		return ranges[index];
}
/** Provides access to range information
* @param rng fills the parameter by current range information
*/
void SonarSensor::getRanges(int rng[])
{
	this->updateSensor();
	for (int i = 0; i < 16; i++)
		rng[i] = this->ranges[i];
}
/** Returns maximum range of sonar
* @param index fills index adress with index of max range
* @return maximum range
*/
float SonarSensor::getMax(int& index)
{
	this->updateSensor();
	float max = ranges[0]; index = 0;
	for (int i = 1; i < 16; i++)
		if (max < ranges[i]) { max = ranges[i]; index = i; }
	return max;
}
/** Returns minimum range of sonar
* @param index fills index adress with index of min range
* @return minimum range
*/
float SonarSensor::getMin(int& index)
{
	this->updateSensor();
	float min = ranges[0]; index = 0;
	for (int i = 1; i < 16; i++)
		if (min > ranges[i]) { min = ranges[i]; index = i; }
	return min;
}
/** Updates range information by using getSonarRange() from PioneerRobotAPI
*/
void SonarSensor::updateSensor()
{
	this->robotAPI->getSonarRange(this->ranges);
}
/** operator [] overloding for getRange()
* @param i index of wanted range
* @return range of given index
*/
float SonarSensor::operator[](int i)
{
	return this->getRange(i);
}
/** Returns angle of given index
* @param index index of wanted angle
* @return angle of index
*/
float SonarSensor::getAngle(int index)
{
	this->updateSensor();
	if (index < 0 || index > 15)
		throw std::string("index must be between 0-15");
	else
		return this->angles[index] /* + this->getTh()*/;
}
