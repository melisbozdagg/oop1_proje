#include "Encryption.h"
/** Encrypts given code
* @param code takes access code array dimention of 4 arr[4]
*/
void Encryption::encrypt(int code[]) {
	int arr[10] = { 7, 8, 9, 0, 1, 2, 3, 4, 5, 6 };

	std::swap(code[0], code[2]);
	std::swap(code[1], code[3]);

	for (int i = 0; i < 4; i++)
		code[i] = arr[code[i]];
}
/** Decrypts given code
* @param enCode takes the original code 
* @param code decrypted code will be written on this parameter
*/
void Encryption::decrypt(int encCode[], int code[]) {
	for (int i = 0; i < 4; i++)
		code[i] = encCode[i];

	int arr[10] = { 3, 4, 5, 6, 7, 8, 9, 0, 1, 2 };

	std::swap(code[0], code[2]);
	std::swap(code[1], code[3]);

	for (int i = 0; i < 4; i++)
		code[i] = arr[code[i]];

}