#include "MotionMenu.h"

void MotionMenu::startMotionMenu()
{
	string pref;

	while (true) {
		cout << endl << "Motion menu" << endl;
		cout << "1. Forward Move Robot" << endl;
		cout << "2. Backward Move Robot" << endl;
		cout << "3. Turn Left" << endl;
		cout << "4. Turn Right" << endl;
		cout << "5. Forward" << endl;
		cout << "6. Move Distance" << endl;
		cout << "7. Stop Robot" << endl;
		cout << "8. Back" << endl;
		cout << "Choose one: " << endl;
		cin >> pref;

		if (pref == "1") {
			int speed;
			cout << "Enter speed(mm/sec): "; cin >> speed;
			this->robotControl->forward(speed);
		}
		else if (pref == "2") {
			int speed;
			cout << "Enter speed(mm/sec): "; cin >> speed;
			this->robotControl->backward(speed);
		}
		else if (pref == "3") {
			this->robotControl->turnLeft();
		}
		else if (pref == "4") {
			this->robotControl->turnRight();
		}
		else if (pref == "5") {
			this->robotControl->stopTurn();
		}
		else if (pref == "6") {
			float distance;int time;
			cout << "Enter distance(mm): "; cin >> distance;
			time = distance; //distance / 1 (mm/sec)
			this->robotControl->forward(1000); // 1 mm/sec
			Sleep(time); 
			this->robotControl->stopMove();
		}
		else if (pref == "7") {
			this->robotControl->stopMove();
		}
		else if (pref == "8") {
			break;
		}
		else
			cout << "Invalid input!" << endl;
	}
}
