/** 
*  @author Muhammed Talha Sahin
*  @date January, 2021
* 
*  main program
*/
#if 1
#include "Menu.h" 
int main() 
{ 
	Menu menu;
	menu.start();
	
	return 0; 
}
#endif