#include "Record.h"

/** Opens file for writing or reading
*  @return true if file successfully opened
*/
bool Record::openFile()
{
	outfile.open(this->fileName);
	inpfile.open(this->fileName);
	if (outfile.is_open() && inpfile.is_open())
		return true;
	return false;
}

/** Closes the opened file
*  @return true if file successfully closed
*/
bool Record::closeFile()
{
	outfile.close();
	inpfile.close();
	if (!outfile.is_open() && !inpfile.is_open())
		return true;
	return false;
}

/** Sets file name 
*  @param name for doing operation in specific file
*/
void Record::setFileName(string name)
{
	this->fileName = name;
}

/** Read lines from the file
*  @return str after readed from file
*/
string Record::readLine()
{
	string str;
	getline(inpfile, str);
	return str;
}

/** Writes inside the file
*  @param str for inputing file
*/
void Record::writeLine(string str)
{
	outfile << str << endl;
}

/**  Gets file for Path.cpp
*  @return outfile 
*/
ofstream* Record::getFile()
{
	return &this->outfile;
}

/** Output operator overloading
*  @param rec for file control
*  @param str for writing string inside the file
*/
Record& operator<<(Record& rec, string& str) {
	rec.writeLine(str);
	return rec;
}

/** Output operator overloading
*  @param rec for file control
*  @param str for reading string inside the file
*/
Record& operator>>(Record& rec, string& str) {
	str = rec.readLine();
	return rec;
}