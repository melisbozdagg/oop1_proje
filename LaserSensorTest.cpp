#if 0

/**
*  @Author Ece Melis Bozdag
*  @date January, 2021
*/

#include <iostream>
#include "LaserSensor.h"
using namespace std;

void Test1();

int main() {

	try {
		Test1();
	}
	catch (string msg) {
		cout << "EXCEPTION: " << msg << endl;
	}

	return 0;
}

void Test1() {

	PioneerRobotAPI* robot = new PioneerRobotAPI;
	float laserData[181];
	robot->connect();

	robot->moveRobot(1000);
	robot->turnRobot(PioneerRobotAPI::DIRECTION::right);
	Sleep(200);
	LaserSensor LSensor(robot);
	cout << "range[64] = " << LSensor.getRange(64) << endl;
	cout << "range[64] = " << LSensor[64] << endl;

	int index = 0;
	cout << "Min range: " << LSensor.getMin(index) << " Index of min range: " << index << endl;
	cout << "Max range: " << LSensor.getMax(index) << " Index of max range: " << index << endl;

	float angle = 0;
	cout << "Angle of index 43: " << LSensor.getAngle(120) << endl;
	cout << "Closest range between 35 and 98: " << LSensor.getClosestRange(35, 98, angle) << " Angle of the closest range: " << angle << endl;


	LaserSensor LSensor1(robot);
	cout << "range[64] = " << LSensor1.getRange(64) << endl;
	angle = 0;
	cout << "Angle of index 43: " << LSensor1.getAngle(190) << endl;
	cout << "Closest range between 35 and 98: " << LSensor1.getClosestRange(35, -98, angle) << " Angle of the closest range: " << angle << endl;
	robot->disconnect();
}


#endif