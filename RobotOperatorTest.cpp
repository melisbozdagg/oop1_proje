/**
*  @Author Muhammed Talha Sahin
*  @date January, 2021
* 
*  RobotOperator test program
*/
#if 0
#include <iostream>
#include "RobotOperator.h"
using namespace std;

void Test1();
void Test2();

int main() {

	try {
		Test1();
		Test2();
	}
	catch (string msg) {
		cout << "EXCEPTION: " << msg << endl;
	}


	system("pause");
}

void Test1() {
	RobotOperator test("Talha","Sahin","0235");

	if (test.checkAccessCode("4793"))
		cout << "Access Granted" << endl;
	else
		cout << "Access Denied" << endl;

	test.print();
	cout << endl;

	if (test.checkAccessCode("0235"))
		cout << "Access Granted" << endl;
	else
		cout << "Access Denied" << endl;
	
	test.print();
}

void Test2() {
	RobotOperator test("Talha", "Sahin", "1521");
	cout << endl << endl;
	if (test.checkAccessCode("26"))
		cout << "Access Granted" << endl;
	else
		cout << "Access Denied" << endl;
	test.print();
}


#endif