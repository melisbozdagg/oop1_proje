#include "Path.h"

/** Adds new pose at the end of the node
* @param pose for adding inside the linkedlist
*/
void Path::addPos(Pose pose)
{
	Node* node = new Node(pose);
	if (this->head == NULL) 
		this->head = this->tail = node;
	else {
		this->tail->next = node;
		this->tail = this->tail->next;
	}
	this->number++;
}

/** Prints all position lists
*/
void Path::print() const
{
	cout << *this;
}

/** Prints all position lists in to file 
* @param file required for printing inside the .txt
*/
void Path::printToFile(ofstream* file)
{
	*file << *this;
}

/** Overloading the subscript operator
* @param i gives the wanted location for pose
* @return the wanted pose information
*/
Pose Path::operator[](int i)
{
	return this->getPos(i);
}

/** getting the desired pose from node
* @param i gives the wanted location
* @return the wanted pose information
*/
Pose Path::getPos(int index)
{
	Node* temp = this->head;
	int count = 0;
	while (count != index) {
		temp = temp->next;
		count++;
	}
	return temp->pose;
}

/** Remove the derised pose from node
* @param index for removing pose in that location after deletion it fills empty space in node
* @retun true if the removing is successful
*/
bool Path::removePos(int index)
{
	if (this->head == NULL && this->tail == NULL)
		return false;

	Node* temp = head, *del;

	if (index == 0) {
		del = this->head;
		this->head = this->head->next;
		delete del;
		return true;
	}
	for (int i = 0; i < index - 1; i++)
	{
		temp = temp->next;
	}
	if (index == this->number) 
		this->tail = temp;
	
	del = temp->next;
	temp->next = temp->next->next;
	delete del;

	this->number--;
	return true;
}
/** Inserts new pose inside the node 
* @param index required to add to the desired location
* @param pose need for insertion
* @return true if the insertion is successful
*/
bool Path::insertPos(int index, Pose pose)
{
	if (index < 0 && index > this->number)
		return false;

	Node* nNode = new Node(pose);
	if (index == 0) {
		nNode->next = this->head;
		this->head = nNode;
	}
	else {
		Node* temp = this->head;
		int i;
		for (i = 0; i < index; i++)
		{
			temp = temp->next;
		}
		if (index == this->number) 
			this->tail = nNode;
		
		nNode->next = temp->next;
		temp->next = nNode;
	}
	this->number++;
	return true;
}

/** Overloading the output operator
* @param out required for cout proccess
* @param path is the main thing that we output
* @return out for writing 
*/
ostream& operator<<(ostream& out, const Path& path) {
	int i = 0;
	Node* temp = path.head;
	while (temp != NULL) {
		out << "Pose " << i << " x:" << temp->pose.getX() << " y:" << temp->pose.getY() << " th:" << temp->pose.getTh() << endl;
		i++;
		temp = temp->next;
	}
	delete temp;
	return out;
}

/** Overloading the input operator
* @param path required for insertion
* @param pose for adding inside the path
* @return path 
*/
Path& operator>>(Path& path, Pose pose) {
	float _x, _y, _th;
	cin >> _x >> _y >> _th;
	pose.setX(_x); pose.setY(_y); pose.setTh(_th);
	path.addPos(pose);

	return path;
}