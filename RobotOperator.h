#pragma once
#include <iostream>
#include <string>
#include "Encryption.h"
using namespace std;
/**
*  @Author Muhammed Talha Sahin
*  @date January, 2021
*
*  RobotOperator class, takes and hides the encrpted access code with username
*/
class RobotOperator {
private:
	string name;
	string surname;
	int accessCode[4];
	bool accessState = false;
public:
	/** constructor takes information to build class
	* @param _name name of person
	* @param _surname surname of person
	* @param _accesscode access code of person
	*/
	RobotOperator(string _name, string _surname, string _accessCode){
		this->name = _name; this->surname = _surname; this->encryptCode(_accessCode);
	}
	void encryptCode(string);
	void decryptCode(int code[]);
	bool checkAccessCode(string);
	void print();
};