#include "Pose.h"

/**
*   Gets coordinate of x-axis 
*   @return x location of robot
*/
float Pose::getX() const
{
	return this->x;
}

/**
*   Sets coordinate of x-axis
*   @param _x coordinate of the robot's x-axis
*/
void Pose::setX(float _x)
{
	this->x = _x;
}

/**
*   Gets coordinate of y-axis
*   @return y location of robot
*/
float Pose::getY() const
{
	return this->y;
}

/**
*   Sets coordinate of y-axis
*   @param _y coordinate of the robot's y-axis
*/
void Pose::setY(float _y)
{
	this->y = _y;
}

/**
*   Gets angle of robot
*   @return th angle of robot
*/
float Pose::getTh() const
{
	return this->th;
}

/**
*   Sets the angle of the robot
*   @param _th angle of the robot
*/
void Pose::setTh(float _th)
{
	if (_th > 180 || _th < -180)
		throw std::string("angle must be smaller than 180 and bigger than -180");
	else
		this->th = _th;
}

/**
*  Operator overloading for is equal(robot's x and y axis and angle)
*  @param other keeps the information of the position to be compared
*  @return true If the comparison is correct
*/
bool Pose::operator==(const Pose& other)
{
	if (other.getX() == this->x && other.getY() == this->y && other.getTh() == this->th)
		return true;
	return false;
}

/**
*  Operator overloading for adding(robot's x and y axis and angle)
*  @param other keeps the information of the position to be added
*  @return nP added position
*/
Pose Pose::operator+(const Pose& other)
{
	Pose nP;
	nP.setX(this->x + other.getX());
	nP.setY(this->y + other.getY());
	nP.setTh(this->th + other.getTh()); 

	return nP;
}

/**
*  Operator overloading for subtracting(robot's x and y axis and angle)
*  @param other keeps the information of the position to be substracted
*  @return nP substracted position
*/
Pose Pose::operator-(const Pose& other)
{
	Pose nP;
	nP.setX(this->x - other.getX());
	nP.setY(this->y - other.getY());
	nP.setTh(this->th - other.getTh()); 

	return nP;
}

/**
*  Operator overloading for adding on(robot's x and y axis and angle)
*  @param other keeps the information of the position to be added on
*  @return nP added on position
*/
Pose& Pose::operator+=(const Pose& other)
{
	this->setX(this->x + other.getX());
	this->setY(this->y + other.getY());
	this->setTh(this->th + other.getTh()); 

	return *this;
}

/**
*  Operator overloading for subtracting on(robot's x and y axis and angle)
*  @param other keeps the information of the position to be substracted on
*  @return nP substracted on position
*/
Pose& Pose::operator-=(const Pose& other)
{
	this->setX(this->x - other.getX());
	this->setY(this->y - other.getY());
	this->setTh(this->th - other.getTh()); 

	return *this;
}

/**
*  Operator overloading for comparing two positions
*  @param other holds the position that will be compared
*  @return true if first one is smaller than second
*/
bool Pose::operator<(const Pose& other)
{
	if (pow(this->x, 2) + pow(this->y, 2) < pow(other.getX(), 2) + pow(other.getY(), 2))
		return true;
	return false;
}

/**
*  Gets the position location of the robot
*  @param _x holds the x-axis location
*  @param _y holds the y-axis location
*  @param _th holds the angle of the robot
*/
void Pose::getPose(float& _x, float& _y, float& _th)
{
	_x = this->x;
	_y = this->y;
	_th = this->th;
}

/**
*  Sets the position location of the robot and holds in private
*  @param _x holds the x-axis location
*  @param _y holds the y-axis location
*  @param _th holds the angle of the robot
*/
void Pose::setPose(float _x, float _y, float _th)
{
	this->setX(_x);
	this->setY(_y);
	this->setTh(_th);
}

/**
*  Calculates the distance between two positions
*  @param pos one of the two positions
*  @return  distance between two positions
*/
float Pose::findDistanceTo(Pose pos)
{
	float distance;
	distance = sqrt(pow(this->x - pos.getX(), 2) + pow(this->y - pos.getY(), 2));
	return distance;
}

/**
*  Calculates the angle between two positions
*  @param pos one of the two positions
*  @return angle between two positions
*/
float Pose::findAngleTo(Pose pos)
{
	float angle;
	angle = 180 - asin(abs(this->y-pos.getY())/this->findDistanceTo(pos));
	return angle;
}
