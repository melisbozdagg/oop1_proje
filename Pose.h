#pragma once
#include <cmath>
#include <iostream>
/**
*   @Author Mislina Cetiner
*   @date January, 2021
*
*	This class has the task of holding and managing the robot's location information.
*/

class Pose {
private:
	float x;
	float y;
	float th;
public:
	/** constructor sets default 0 or given poisitons
	*/
	Pose(float _x = 0, float _y = 0, float _th = 0) {
		this->setX(_x); this->setY(_y); this->setTh(_th);
	}
	float getX() const;
	void setX(float);
	float getY() const;
	void setY(float);
	float getTh() const;
	void setTh(float);
	bool operator==(const Pose& other);
	Pose operator+(const Pose& other);
	Pose operator-(const Pose& other);
	Pose& operator+=(const Pose& other);
	Pose& operator-=(const Pose& other);
	bool operator<(const Pose& other);
	void getPose(float& _x, float& _y, float& _th);
	void setPose(float _x, float _y, float _th);
	float findDistanceTo(Pose pos);
	float findAngleTo(Pose pos);
};