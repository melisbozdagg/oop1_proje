#include "LaserSensor.h"

/**
*  Returns distance information of the sensor with index.
*  @param index keeps information about which sensor it is
*  @return range[index] distance information of the sensor
*/
float LaserSensor::getRange(int index)
{
	this->updateSensor();
	if (index < 0 || index > 180)
		throw std::string("index must be between 0-180");
	else
		return this->ranges[index];
}

/**
*  Returns all distance informations about sensors
*  @param rng[] is array that holds sensors
*  @return ranges[] is array thaat holds all distance information about sensors
*/
void LaserSensor::getRanges(int rng[])
{
	this->updateSensor();
	for (int i = 0; i < 181; i++)
		rng[i] = this->ranges[i];
}

/**
*   Uploads the robot's current sensor distance values to the array.
*/
void LaserSensor::updateSensor()
{
	//this->robotAPI->getLaserRange(this->ranges);
}

/**
*  Returns the maximum of the sensor distance values.
*  @param index Returns the index of data with the maaximum distance
*  @return max returns the distance value
*/
float LaserSensor::getMax(int& index)
{
	this->updateSensor();
	float max = ranges[0]; index = 0;
	for (int i = 1; i < 181; i++)
		if (max < ranges[i]) { max = ranges[i]; index = i; }
	return max;
}

/**
*  Returns the minimum of the sensor distance values.
*  @param index Returns the index of data with the minimum distance
*  @return min returns the distance value
*/
float LaserSensor::getMin(int& index)
{
	this->updateSensor();
	float min = ranges[0]; index = 0;
	for (int i = 1; i < 181; i++)
		if (min > ranges[i]) { min = ranges[i]; index = i; }
	return min;
}

/**
*   Overloading the subscript operator
*   @param i gives the wanted range
*   @return the wanted range information
*/
float LaserSensor::operator[](int i)
{
	return this->getRange(i);
}

/**
*   Returns angle information of the sensor with index.
*	@param index gives the wanted location for angle
*   @return the wanted angle information
*/
float LaserSensor::getAngle(int index)
{
	this->updateSensor();
	if (index < 0 || index > 180)
		throw std::string("index must be between 0-180");
	else
		return index; 
}

/**
*  Between startAngle and endAngle angles returns the angle of the smallest distance
*  @param startAngle starting point for closest range search
*  @param endAngle ending point for closest range search
*  @param angle for returning the smallest angle value
*  @return min holds and returns the clossest range
*/
float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle)
{
	this->updateSensor();
	if (startAngle < 0 || startAngle > 180 || endAngle < 0 || endAngle > 180)
		throw std::string("index must be between 0-180");
	else {
		float min = this->ranges[(int)startAngle];
		angle = (int)startAngle;
		for (int i = (int)startAngle + 1; i <= (int)endAngle; i++)
		{
			if (min > this->ranges[i]) {
				min = this->ranges[i];
				angle = i;
			}
		}
		return min;
	}
}
