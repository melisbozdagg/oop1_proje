﻿#pragma once
#include <iostream>
#include "PioneerRobotAPI.h"
#include "Pose.h"
#include "Path.h"
using namespace std;
/**
*  @Author Muhammed Talha Sahin
*  @date January, 2021
*
*  RobotControl class, makes the robot move and holds position information
*/
class RobotControl : private PioneerRobotAPI{
private:
	Pose* position;
	PioneerRobotAPI* robotAPI;
	Path* path;
	int state = 0;
public:
	/** constructor, creates objects and uses setRobot from PioneerRobotAPI to update positions
	* @param robot takes a PioneerRobotAPI pointer to use
	*/
	RobotControl(PioneerRobotAPI* robot) {
		this->robotAPI = robot;
		position = new Pose;
		path = new Path;
		this->robotAPI->setRobot(this);
	}
	/** destructor, deletes pointers
	*/
	~RobotControl() {
		delete position;
		delete path;
	}
	void turnLeft(); 
	void turnRight(); 
	void forward(float); 
	void backward(float); 
	Pose getPose(); 
	void setPose(Pose); 
	void stopTurn(); 
	void stopMove();
	void print();
	virtual void updateRobot();
};

