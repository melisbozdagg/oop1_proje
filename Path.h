﻿#pragma once
/** 
*  @Author Huseyin Can Ergun
*  @date January, 2021
* 
*  Robot's path class. Stores pose information for writing inside the file
*/
#include "Pose.h"
#include "Record.h"
#include <iostream>
using namespace std;
/** node struct of pose
*/
struct Node {
	Node(Pose pos) { pose = pos; next = NULL; }
	Node* next;
	Pose pose;
};

class Path {
	friend ostream& operator<<(ostream&, const Path&);
	friend Path& operator>>(Path&, Pose);
private:
	Node* tail;
	Node* head;
	int number;
	Record* rec;
	void printToFile(ofstream*);
public:
	/** constructor 
	*/
	Path() {
		this->tail = NULL;
		this->head = NULL;
		this->number = 0;
		rec = new Record;
	}
	/** destructor
	*/
	~Path() {
		rec->setFileName("Record.txt");
		if (!rec->openFile()) cout << "File could not opened!!" << endl;
		this->printToFile(rec->getFile());
		rec->closeFile();
	}
	void addPos(Pose pose);
	void print() const;
	Pose operator[](int i);
	Pose getPos(int index);
	bool removePos(int index);
	bool insertPos(int index, Pose pose);
};
