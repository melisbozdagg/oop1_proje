#if 0

/**
*  @Author H�seyin Can Erg�n
*  @date January, 2021
*/

#include <iostream>
#include "Path.h"
#include "Pose.h"
#include <string>
using namespace std;

void Test1();

int main() {

	Test1();

	system("pause");
}
void Test1() {
	Path test;
	Pose coor;
	coor.setPose(10, 20, 45);
	test.addPos(coor);

	test.print(); cout << endl;
	coor.setPose(25, 45, 90); test.addPos(coor);
	coor.setPose(37, 56, 45); test.addPos(coor);
	coor.setPose(67, 89, 20); test.addPos(coor);
	
	test.print(); cout << endl;

	coor = test.getPos(3);
	cout << "x: " << coor.getX() << " y: " << coor.getY() << " th: " << coor.getTh(); cout << endl;

	coor = test[3];
	cout << endl; 

	coor.setPose(110, 42, 120); test.addPos(coor);
	coor.setPose(60, 26, 21); test.addPos(coor);
	coor.setPose(90, 50, 10); test.addPos(coor);
	test.print();

	test.removePos(3);

	cout << endl << "REMOVED: " << endl; test.print();

	coor.setPose(100, 100, 100);
	test.insertPos(3, coor);

	cout << endl << "INSERTED: " << endl;; test.print();

	
	test >> coor;

	cout << endl << "INSERTED WITH OPERATOR: " << endl;; test.print();

	cout << test;
}

#endif