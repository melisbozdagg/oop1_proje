#pragma once
#include "PioneerRobotAPI.h"
/**
*  @Author Fethi Albayram
*  @date January, 2021
*
* SonarSensor class. Holding and returning of sonar information of robot
*/
class SonarSensor {
private:
	float ranges[16] = {0};
	PioneerRobotAPI* robotAPI;
	const int angles[16] = { -90,-50,-30,-10,10,30,50,90,90,130,150,170,-170,-130,-90 };
	void updateSensor();
public:
	SonarSensor(PioneerRobotAPI* robot) {
		this->robotAPI = robot;
	}
	float getRange(int index);
	void getRanges(int rng[]);
	float getMax(int& index);
	float getMin(int& index);
	float operator[](int i);
	float getAngle(int index);
};