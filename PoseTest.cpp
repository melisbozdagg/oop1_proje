#if 0

/**
*   @Author Mislina �etiner
*   @date January, 2021
*/

#include <iostream>
#include "Pose.h"
using namespace std;

void Test1();
void Test2();

int main() {

	try {
		cout << endl << "--Test1--" << endl;
		Test1();
		cout << endl << "--Test2--" << endl;
		Test2();
		cout << endl << "--End of Test--" << endl;
	}catch (string msg) {
		cout << "EXCEPTION: " << msg << endl;
	}
	return 0;
}

void print(Pose pose) {
	cout << "x: " << pose.getX() << " y: " << pose.getY() << " th: " << pose.getTh() << endl;
}

void Test1() {
	Pose pose1;
	cout << "pose1 - "; print(pose1);
	pose1.setX(2); pose1.setY(5); pose1.setTh(30);
	cout << "pose1 - "; print(pose1);

	Pose pose2(5, 8, 50);
	cout << "pose2 - "; print(pose2);
	pose2.setPose(2, 5, 30);
	cout << "pose2 - "; print(pose2);

	if (pose1 == pose2) cout << "pose1 and pose2 are equal." << endl;

	Pose poseS;
	poseS = pose1 + pose2;
	pose1 += pose2;

	if (pose1 == poseS) cout << "pose1 and poseS are equal." << endl;

	pose1 -= pose2; print(pose1);

	print(poseS - pose2);

	if(pose1 < pose2) cout << "pose1 < pose2 is true" << endl;
	else cout << "pose1 < pose2 is false" << endl;
}

void Test2() {
	Pose pose1(5, 6, 30), pose2(10, 20, 50);
	cout << "pose1: "; print(pose1); cout << "pose2: "; print(pose2);
	cout << "Distance to pose2 from pose1: " << pose1.findDistanceTo(pose2) << endl;
	cout << "Distance to pose1 from pose2: " << pose2.findDistanceTo(pose1) << endl;
	cout << "Angle to pose2 from pose1: " << pose1.findAngleTo(pose2) << endl;
}

#endif