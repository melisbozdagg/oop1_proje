#pragma once
#include "PioneerRobotAPI.h"
#include <iostream>
/**
*  @Author Ece Melis Bozdag
*  @date January, 2021
*
*  Takes and stores information about robot's laser sensors
*/

class LaserSensor {
private:
	float ranges[181] = {0};
	PioneerRobotAPI* robotAPI;
	
public:
	/** consturctor
	*/
	LaserSensor(PioneerRobotAPI* robot) {
		this->robotAPI = robot;
	}
	float getRange(int index);
	void getRanges(int rng[]);
	float getMax(int& index);
	float getMin(int& index);
	float operator[](int i);
	float getAngle(int index);
	float getClosestRange(float startAngle, float endAngle, float& angle);
	void updateSensor();
};
