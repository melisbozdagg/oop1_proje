#include "RobotControl.h"
/** makes robot turn left
*/
void RobotControl::turnLeft()
{
	this->robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}
/** makes robot turn right
*/
void RobotControl::turnRight()
{
	this->robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}
/** makes robot move forward with given speed
*  @param speed 
*/
void RobotControl::forward(float speed)
{
	if (speed <= 0)
		throw std::string("speed can not be 0 or smaller than 0");
	else {
		this->state = 1;
		this->robotAPI->moveRobot(speed);
	}
}
/** makes robot move backward with given speed
*  @param speed
*/
void RobotControl::backward(float speed)
{
	if (speed <= 0)
		throw std::string("speed can not be 0 or smaller than 0");
	else {
		this->state = 1;
		this->robotAPI->moveRobot(-speed);
	}
}
/** returns position information of robot
*  @return position 
*/
Pose RobotControl::getPose()
{
	return *this->position;
}
/** sets robot position  
*  @param nPose position to set robot
*/
void RobotControl::setPose(Pose nPose)
{
	this->robotAPI->setPose(nPose.getX(), nPose.getY(), nPose.getTh());
}
/** stops the process of turning (turns forward)
*/
void RobotControl::stopTurn()
{
	this->robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
}
/** stops robot and sets state to 0
*/
void RobotControl::stopMove()
{
	this->state = 0;
	this->robotAPI->stopRobot();
}
/** prints poisiton and state information of robot
*/
void RobotControl::print()
{
	cout << "Pos: x = " << this->position->getX() << " y = " << this->position->getY() << " Th = " << this->position->getTh()
		<< "  State: " << this->state << endl;
}
/** updates robot position - virtual inherited function from PioneerRobotAPI
*/
void RobotControl::updateRobot()
{
	this->position->setPose(this->robotAPI->getX(), this->robotAPI->getY(), this->robotAPI->getTh());
	this->path->addPos(*this->position);
}

