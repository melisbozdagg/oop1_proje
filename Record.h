﻿#pragma once
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

/**
*  @Author Huseyın Can Ergun
*  @date January, 2021
*
*  holds records about robot's position and reads/writes in file
*/
class Record {
	friend Record& operator<<(Record&, string&); 
	friend Record& operator>>(Record&, string&); 
private:
	string fileName;
	ofstream outfile;
	ifstream inpfile;
public:
	bool openFile();
	bool closeFile();
	void setFileName(string name);
	string readLine();
	void writeLine(string str);
	ofstream* getFile();

};
