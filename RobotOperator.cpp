#include "RobotOperator.h"

/** Takes and passes access code to Encryption class to encrypt 
* @param code takes code as string to keep possible '0'' at the beginning (ex:0548)
*/
void RobotOperator::encryptCode(string code) {
	Encryption key;
	if(code.size() != 4) throw std::string("Invalid code size");

	for (int i = 0; i < 4; i++)
		this->accessCode[i] = code[i] - '0';

	key.encrypt(this->accessCode);
}
/** Passes encrypted access code to Encryption class to decrypt
* @param code takes access code array dimention of 4 arr[4]
*/
void RobotOperator::decryptCode(int code[]) {
	Encryption key;
	key.decrypt(this->accessCode, code);
}
/** Checks if the access code is true
* @param code takes access code to check
* @return if acces granted(1) or denied(0) (bool)
*/
bool RobotOperator::checkAccessCode(string code) {
	int tempcode[4], decCode[4];
	if (code.size() != 4) throw std::string("Invalid code size");

	for (int i = 0; i < 4; i++)
		tempcode[i] = code[i] - '0';
	this->decryptCode(decCode);
	for (int i = 0; i < 4; i++)
	{
		if (tempcode[i] != decCode[i]) {
			this->accessState = false;
			return false;
		}
	}
	this->accessState = true;
	return true;
}
/** Prints the information of RobotOperator class
*/
void RobotOperator::print() {
	cout << "Name: " << name << endl;
	cout << "Surname: " << surname << endl;
	cout << "Access State: " << accessState << endl;
}