#pragma once
#include <iostream>
/**
*  @Author Muhammed Talha Sahin
*  @date January, 2021
*
*  Encryption class, encrypts and decrypts the code
*/
class Encryption {
public:
	void encrypt(int code[]);
	void decrypt(int encCode[], int code[]);
};