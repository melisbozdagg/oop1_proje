/**
*  @Author Muhammed Talha Sahin
*  @date January, 2021
*/
#if 0
#include <iostream>
#include "RobotControl.h"
using namespace std;

void Test1();
void Test2();

int main() {
	
	try {
		Test1();
		Test2();
	}catch (string msg) {
		cout << "EXCEPTION: " << msg << endl;
	}

	return 0;
}

void Test1() {
	PioneerRobotAPI* robot = new PioneerRobotAPI;
	if (!robot->connect()) {
		cout << "Connection error!" << endl;
		exit(0);
	}

	RobotControl* babur = new RobotControl(robot);

	babur->print();
	babur->forward(1000);
	Sleep(3000);
	babur->print();

	babur->turnLeft();
	Sleep(2000);
	Pose pose = babur->getPose();
	babur->print();

	babur->turnRight();
	Sleep(1000);
	babur->print();

	babur->stopTurn();
	babur->backward(1000);
	Sleep(1000);
	babur->stopMove();
	babur->print();

	babur->forward(1000);
	Sleep(2000);
	babur->print();

	babur->stopMove();
	babur->setPose(pose);
	Sleep(1000);
	babur->print();

	//robot->disconnect();
	delete robot;
	delete babur;
}

void Test2() {
	PioneerRobotAPI* robot = new PioneerRobotAPI;
	if (!robot->connect()) {
		cout << "Connection error!" << endl;
		exit(0);
	}

	RobotControl* babur = new RobotControl(robot);
	babur->print();
	babur->forward(-1000);
	Sleep(3000);
	babur->print();

	robot->disconnect();
	delete robot;
	delete babur;
}

#endif