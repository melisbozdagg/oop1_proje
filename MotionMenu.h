#pragma once
#include <iostream>
using namespace std;
#include "PioneerRobotAPI.h"
#include "RobotControl.h"

/**
*  @Author H�seyin Can Erg�n
*  @date January, 2021
* 
*  motion menu 
*/
class MotionMenu {
private:
	RobotControl* robotControl;
public:
	MotionMenu(PioneerRobotAPI* robot) {
		this->robotControl = new RobotControl(robot);
	}
	~MotionMenu() {
		delete robotControl;
	}
	void startMotionMenu();
};

