#pragma once
#include <iostream>
using namespace std;
#include "PioneerRobotAPI.h"
#include "MotionMenu.h"
#include "SensorMenu.h"
#include "RobotOperator.h"

/**
*  @Author Muhammed Talha Sahin
*  @date January, 2021
* 
*  general menu of the program, asks for password first to access menu
*/
class Menu {
private:
	PioneerRobotAPI* robotAPI;
	MotionMenu* motionMenu;
	SensorMenu* sensorMenu;
	RobotOperator* ROperator;
	bool is_connected = 0;

	void connectionMenu();
	void startMenu();
public:
	/** constructor creates objects
	*/
	Menu() {
		this->robotAPI = new PioneerRobotAPI;
		this->motionMenu = new MotionMenu(this->robotAPI);
		this->sensorMenu = new SensorMenu(this->robotAPI);
		this->ROperator = new RobotOperator("John", "Wick", "0249");
	}
	/** destructor 
	*/
	~Menu() {
		delete motionMenu;
		delete sensorMenu;
		delete ROperator;
	}
	void start();
};

