#include "Menu.h"
/** public starting program to access login program
*/
void Menu::start()
{
	try {
		this->ROperator->print(); cout << "(Access code: 0249)" << endl;
		cout << "Please enter access code: ";
		string accessCode;
		cin >> accessCode;
		if (this->ROperator->checkAccessCode(accessCode)) {
			cout << "Access Granted" << endl << endl;
			this->startMenu();
		}
		else
			cout << "Access Denied" << endl;
	}
	catch (string str) {
		cout << "EXCEPTION: " << str << endl;
	}
}
/** private - starting general menu if access granted
*/
void Menu::startMenu()
{
	string pref;
	while (true) {
		cout << "Main menu" << endl;
		cout << "1. Connection" << endl;
		cout << "2. Motion" << endl;
		cout << "3. Sensor" << endl;
		cout << "4. Quit" << endl;
		cout << "Choose one: ";
		cin >> pref;

		if (pref == "1") {
			this->connectionMenu();
		}
		else if (pref == "2") {
			this->motionMenu->startMotionMenu();
		}
		else if (pref == "3") {
			this->sensorMenu->startSensorMenu();
		}
		else if (pref == "4") {
			cout << "Writing path history to file.." << endl;
			cout << "Ending program..." << endl;
			break;
		}
		else
			cout << "Invalid input!" << endl;
	}
}
/** connection menu
*/
void Menu::connectionMenu() {
	string pref;

	while (true) {
		cout << endl << "Connection menu" << endl;
		cout << "1. Connect Robot" << endl;
		cout << "2. Disconnect Robot" << endl;
		cout << "3. Back" << endl;
		cout << "Choose one: " << endl;
		cin >> pref;

		if (pref == "1") {
			if (this->is_connected == 0)
				if (this->robotAPI->connect())
					cout << endl << "Robot connected." << endl;
				else
					cout << endl << "Connection error!!" << endl;
			else
				cout << endl << "Already connected!!" << endl;
		}
		else if (pref == "2") {
			this->robotAPI->disconnect();
			cout << endl << "Robot disconnected." << endl;
		}
		else if (pref == "3") {
			break;
		}
		else
			cout << "Invalid input!!" << endl;
	}
}