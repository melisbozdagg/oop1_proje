/**
*  @Author Fethi Albayram
*  @date January, 2021
* 
*  SonarSensor test program
*/
#if 0
#include <iostream>
#include "SonarSensor.h"
using namespace std;
void Test1();

int main() {

	try {
		Test1();
	}
	catch (string msg) {
		cout << "EXCEPTION: " << msg << endl;
	}

	return 0;
}

void Test1() {
	PioneerRobotAPI* robot = new PioneerRobotAPI;
	robot->connect();
	SonarSensor sSensor(robot);
	cout << endl << endl <<"Range[14] = " << sSensor.getRange(14) << endl;
	cout << "Range[14] = " << sSensor[14] << endl;
	cout << "Angle of Range[14] is " << sSensor.getAngle(14) << endl;


	SonarSensor sSensor;
	int index;
	float num = sSensor.getMin(index);
	cout << endl << endl << "Min of ranges: Range[" << index << "] = " << num << endl;
	num = sSensor.getMax(index);
	cout << "Max of ranges: Range[" << index << "] = " << num << endl;
}

#endif